package uk.co.iscoding.helloyou;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class NameActivity extends Activity {

    EditText nameBox;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        
        nameBox = (EditText)findViewById(R.id.name_text);
    }

    public void buttonClicked(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("Username", nameBox.getText().toString());
        startActivity(intent);
    }
    
}
