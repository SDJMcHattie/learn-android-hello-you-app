package uk.co.iscoding.helloyou;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Bundle extras = getIntent().getExtras();
        String username = extras.getString("Username");
        
        TextView helloYou = (TextView)findViewById(R.id.hello_you);
        String prefix = getString(R.string.hello_world);
        helloYou.setText(prefix + " " + username);
    }

}
